#!/bin/bash

set -e

echo "(1) remove redmine volume data..."
rm -rf /srv/docker/redmine/

echo "(2) generate secret key..."
unset SENTRY_SECRET_KEY
SENTRY_SECRET_KEY=`docker run --rm sentry config generate-secret-key`
echo $SENTRY_SECRET_KEY
export SENTRY_SECRET_KEY=$SENTRY_SECRET_KEY
sed -i.bak '/SENTRY_SECRET_KEY/d' ~/.bashrc
echo "export SENTRY_SECRET_KEY='$SENTRY_SECRET_KEY'" >> ~/.bashrc

echo "(3) docker-compose up redis postgres smtp memcached sentry..."
docker-compose up -d redis postgres smtp memcached sentry

echo "    Waiting redis ready."
while [ -z "$(docker logs sentry-redis 2>&1 | grep 'The server is now ready to accept connections on port 6379')" ]; do
    echo -ne "."
    sleep 1
done
echo ""

echo "    Waiting postgres ready."
while [ -z "$(docker logs sentry-postgres 2>&1 | grep 'autovacuum launcher started')" ]; do
    echo -ne "."
    sleep 1
done
echo ""

echo "    Waiting sentry ready."
while [ -z "$(docker logs sentry 2>&1 | grep 'ready in')" ]; do
    echo -ne "."
    sleep 1
done
echo ""

sleep 10

#no create user
echo "(4) Sentry upgrade..."
docker-compose run sentry sentry upgrade << EOF
Y
admin@rakr.info
1qaz@WSX
1qaz@WSX
y
EOF

docker login -u=timmyEvil -p=1qaz@WSX registry.gitlab.com

echo "(5) docker compose up all services..."
docker-compose up -d

echo "    Waiting redmine postgres ready."
while [ -z "$(docker logs redmine-postgresql 2>&1 | grep 'autovacuum launcher started')" ]; do
    echo -ne "."
    sleep 1
done
echo ""

echo "    Waiting redmine ready."
while [ -z "$(docker logs redmine 2>&1 | grep 'nginx entered RUNNING state')" ]; do
    echo -ne "."
    sleep 1
done
echo ""


# init Redmine data.
echo "(6) init redmine data for Rakr..."
docker cp ./initDB.sh redmine-postgresql:/initDB.sh
docker exec -d redmine-postgresql sh /initDB.sh
echo ""
echo "************************************************************************"
echo "Great!! All the services are running:"
echo ""
echo "   Rakr: http://localhost"
echo "Redmine: http://localhost:8082 (admin/1qaz@WSX)"
echo " Sentry: http://localhost:8080 (admin@rakr.info/1qaz@WSX)"
echo ""
echo "Please relogin for reload SENTRY_SECRET_KEY variable or execute command:"
echo "      #> source ~/.bashrc"
echo ""
echo "Now you can start/stop servies by these commands:"
echo " stop all services: docker-compose stop"
echo "start all services: docker-compose start"
echo "************************************************************************"
