# Rakr Docker Compose 

## Requirements
 * Docker 1.10.0+
 * Compose 1.6.0+

## Init
Install and start all services including sentry, redmine and Rakr.
```sh
sudo ./startAllFirst.sh
```
## stop/start all services
```sh
docker-compose stop
docker-compose start
```
## destroy data before re-initialize.
```sh
sudo ./destroyAll.sh
```
## Services URL
```
   Rakr: http://localhost
Redmine: http://localhost:8082 (admin/1qaz@WSX)
 Sentry: http://localhost:8080 (admin@rakr.info/1qaz@WSX)
```
