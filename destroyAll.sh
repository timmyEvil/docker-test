#!/bin/bash

rm -rf /srv/docker/redmine/
docker rm -f -v $(sudo docker ps -qa --filter=label=tag=rakr)
sed -i.bak '/SENTRY_SECRET_KEY/d' ~/.bashrc
